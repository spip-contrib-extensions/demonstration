# Démonstration

![logo plugin demonstration](prive/themes/spip/images/demo-xx.svg)

À partir de **Base de démarrage** par [tétue](https://contrib.spip.net/Base-de-demarrage)

## Utilisation

**Démonstration** propose une base de donnée et le dossier IMG nécessaire au bon fonctionnement d'un SPIP de démonstration. 

Par défaut, il y a 4 utilisateur⋅ice⋅s avec les différents niveaux de droits possibles (Administrateur⋅ice webmestre, administrateur⋅ice sans webmestre, rédacteur⋅ice, visiteur⋅ice), si vous préférez conserver les utilisateur⋅ice⋅s déjà créé, utiliser la base de donnée *mini*.
Dans le cas de l'utilisation de la base complète, les informations de connexions sont affichées dans l'article 1 : Accueil (`ndd.org/spip.php?article1`)
