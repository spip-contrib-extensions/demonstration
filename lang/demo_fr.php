<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
//
// spip-core
//
// T
'titre_page_accueil' => 'Démonstration',
// S
'spip_42' => 'Pour que SPIP resplendisse !',
'spip42_info' => 'Actuellement, ce plugin est en cours de développement. Vous pouvez trouver toutes les informations pour l utiliser sur la page de documentation du plugin.',
'demo_menu' => 'Démonstration',
);